/**
 * Programa en donde se puede escoger la opcion de viaje
 * se ingresa la opcion de forma numérica del destino y la edad.
 * se agrega el descuento, se hace la conversión
 * y se agrega el IVA
 * mostrando el destino, el descuento
 * tipo de cliente, precio parcial, conversion a pesos mexicanos, mostrando el
 * IVA y mostrando el precio total del viaje.
 * @author jose angel juarez ortiz
 */
import javax.swing.JOptionPane; 

public class PrecioRutas {
    int opcion, edad, operacion, precio;
    double age, conv, total;

    public static void main(String[] args) {
            PrecioRutas pre = new PrecioRutas();
            pre.mostrarPrecios();
            pre.escogerRuta();
            pre.asignarDato();
            pre.preguntarEdad();
            pre.hacerDescuento();
            pre.hacerOperacion();
            pre.hacerConversion();
            pre.mostrarParcial();
            pre.agregaIva();
            pre.mostrarTotal();
    }
    //Método para mostar los precios disponibles
    public void mostrarPrecios(){
        System.out.println(" ________________________________________________________ ");
        System.out.println("|                  Ruta               |       Costo      |");
        System.out.println("|--------------------------------------------------------|");
        System.out.println("|1. México-New York, New York-México  |   $100 USD       |");
        System.out.println("|2. México-París, París-México        |   $800 Eur       |");
        System.out.println("|3. México-Madrid, Madrid-México      |   $650 Eur       |");
        System.out.println("|4. México-Atenas, Atenas-México      |   $970 Eur       |");
        System.out.println("|5. México-Sao Paulo, Sao Paulo-México|   $12,500 MXN    |");
        System.out.println("|_____________________________________|__________________|");
        System.out.println("|   MXN = 1         |     Eur = 22.24 |      USD = 17.80 |");
        System.out.println("|___________________|_________________|__________________|");
    }
    //Metodo en donde se escoge la ruta a la que se desea viajar
    public void escogerRuta(){
        opcion= Integer.parseInt(JOptionPane.showInputDialog("Escoja la ruta acorde\nel numero de opcion: "));
    }
    //Método que guarda la informacion del la opcion seleccionada con el teclado
    public void asignarDato(){
        switch(opcion){
            case 1:
                opcion= 1;
                System.out.println("El destino que usted escogio es México New York");
                precio=100;
                break;
            case 2:
                opcion= 2;
                System.out.println("El destino que ustes escogio es México París");
                precio=800;
                break;
            case 3:
                opcion= 3;
                System.out.println("El destino que usted escogio es México Madrid");
                precio=650;
                break;
            case 4:
                opcion= 4;
                System.out.println("El destino que usted escogio es México Atenas");
                precio=970;
                break;
            case 5:
                opcion= 5;
                System.out.println("El destino que usted escogio es México Sao Paulo");
                precio=12500;
                break;
                
            default:
                System.out.println("Escoja una opcion dentro de la lista");
        }
    }
    //metodo que hace la pregunta de la edad
    public void preguntarEdad(){
        edad = Integer.parseInt(JOptionPane.showInputDialog("Por favor, digite su edad: "));
    }
    //metodo que asigna el descuento del viaje
    public void hacerDescuento(){
        if(edad>=60){
            System.out.println("Usted es mayor de 60 años, tiene un descuento del 60%");
            age=0.6;
        }
        if(edad>=18 && edad<=29){
            System.out.println("Usted tiene un descuento de 35% por ser estudiante");
            age=0.35;
        }
        if(edad>=30 && edad<=59){
            System.out.println("Usted es cliente regular por lo tanto no hay descuento");
            age=0;
        }
    }
    //metodo que hace la operacion de los viajes
    public void hacerOperacion(){
        operacion = (int) ((int) (precio) - (age*precio));
        System.out.println("El precio parcial es de : $"+ operacion);
    }
    //metodo que hace la conversion de los costos de los viajes
    public void hacerConversion(){
        switch(opcion){
            case 1:
                opcion=1;
                conv= operacion*17.80;
                break;
            
            case 2:
                opcion=2;
                conv=operacion*22.24;
                break;
                
            case 3:
                opcion=3;
                conv=operacion*22.24;
                break;
                
            case 4:
                opcion=4;
                conv=operacion*22.24;
                break;
                
            case 5:
                opcion=5;
                conv=operacion*1;
                break;
                
            default:        
        }
    }
    //metodo que muestra el precio parcial en pesos mexicanos del viaje.
    public void mostrarParcial(){
        System.out.println("El precio parcial en pesos mexicanos es de: $"+ conv);
    }
    //metodo que agrega el iva del 16%
    public void agregaIva(){
        total= conv + conv*0.16;
    }
    //metodo que muestra el iva  precio total del viaje
    public void mostrarTotal(){
        System.out.println("+ el IVA del 16% es de "+ conv*0.16);
        System.out.println("el precio total es de: "+ total);
    }
}